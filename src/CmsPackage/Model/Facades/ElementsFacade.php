<?php

namespace Packages\CmsPackage\Model\Facades;

use movi\Model\Query;
use movi\Model\TranslationsFacade;
use Packages\CmsPackage\Model\Entities\Element;
use Packages\CmsPackage\Model\Entities\Route;

class ElementsFacade extends TranslationsFacade
{

    public function findByIdOrAlias($input)
    {
        $query = new Query();
        $query->where('@id = %i OR @alias = %s', $input, $input);

        return $this->find($query);
    }

} 