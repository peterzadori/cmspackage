<?php

namespace Packages\CmsPackage\Model\Facades;

use Packages\CmsPackage\Model\Entities\Route;
use movi\Model\Query;

trait RouteAwareFacade
{

	public function findByRoute(Route $route)
	{
		$query = new Query();
		$query->restrict('route', $route);

		return $this->find($query);
	}

} 