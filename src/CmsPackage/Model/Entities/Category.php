<?php

namespace Packages\CmsPackage\Model\Entities;

use movi\Model\Entities\TreeEntity;

/**
 * Class Category
 * @package Packages\CmsPackage\Model\Entities
 *
 * @property Route[] $routes m:belongsToMany
 * @property Category|NULL $parent m:hasOne(parent_id) m:onDelete(CASCADE)
 */
class Category extends TreeEntity
{

}