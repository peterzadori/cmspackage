<?php

namespace Packages\CmsPackage\Model\Entities;

class Shortcut extends Route
{

    public function getIcon()
    {
        return 'fa-mail-forward';
    }

}