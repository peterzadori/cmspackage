<?php

namespace Packages\CmsPackage\Model\Entities;

use movi\Model\Entities\IdentifiedEntity;
use movi\Model\Entities\TranslatableEntity;
use Nette\Utils\Json;

/**
 * Class Element
 * @package Packages\CmsPackage\Model\Entities
 *
 * @property string $type
 * @property string $entity
 *
 * @property string $name
 * @property string $custom_class m:size(64)
 * @property string $alias m:size(32)
 * @property int $margin_top
 * @property int $margin_bottom
 * @property string $wrapper_html
 * @property string $hidden_time
 *
 * @property int $order
 * @property int $grid_column
 * @property string $settings
 * @property string|NULL $block
 *
 * @property Element|NULL $parent m:hasOne(parent_id) m:onDelete(CASCADE)
 * @property Route|NULL $route m:hasOne m:onDelete(CASCADE)
 */
class Element extends TranslatableEntity
{

    public $wrapper = true;


    public function getDefaultSettings()
    {
        return [];
    }


    public function setSettings(array $settings)
    {
        $this->row->settings = Json::encode($settings);
    }


    public function getSettings($asArray = false)
    {
        return Json::decode($this->row->settings, $asArray ? Json::FORCE_ARRAY : 0);
    }

}