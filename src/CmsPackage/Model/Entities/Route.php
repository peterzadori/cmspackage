<?php

namespace Packages\CmsPackage\Model\Entities;

use LeanMapper\Filtering;
use LeanMapper\Fluent;
use movi\Model\Entities\FileEntity;
use movi\Model\Entities\TreeEntity;
use movi\Model\Query;

/**
 * Class Route
 * @package Packages\CmsPackage\Model\Entities
 *
 * @property string $title m:translate
 * @property string $abstract m:translate
 *
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property string $alias m:size(64)
 * @property bool $homepage
 * @property bool $hidden
 *
 * @property string|NULL $image m:size(128) m:file
 * @property string|NULL $template
 * @property bool $show_children
 *
 * @property string $entity
 *
 * @property \DateTime $added
 * @property \DateTime $modified
 *
 * @property Route|NULL $shortcut m:hasOne(shortcut_id) m:onDelete(SET NULL)
 * @property Route|NULL $parent m:hasOne(parent_id) m:onDelete(CASCADE)
 * @property Category[] $categories m:hasMany
 * @property Element[] $elements m:belongsToMany
 *
 * @property array $blocks m:ignore
 */
class Route extends TreeEntity implements FileEntity, ReorderElements
{

    /**
     * @var bool
     */
    protected $canBeViewed = true;


    public function getElements()
    {
        $this->refresh();

        $elements = [];
        $rows = $this->get('elements', [
            'query' => function(Query $query) {
                $query->orderBy('@order ASC');
            }
        ]);

        foreach ($rows as $row)
        {
            $elements[$row->order] = $row;
        }

        return $elements;
    }


    public function getBlocks()
    {
        $this->refresh();

        $elements = [];
        $rows = $this->get('elements', [
            'query' => function(Query $query) {
                $query->orderBy('@order ASC');
            }
        ]);

        foreach ($rows as $row)
        {
            $elements[$row->block][$row->order] = $row;
        }

        return $elements;
    }


    public function getIcon()
    {
        if ($this->homepage) {
            return 'fa-home';
        } else if ($this->hidden) {
            return 'fa-eye-slash';
        } else {
            return 'fa-file-text-o';
        }
    }


    public function canBeViewed()
    {
        return $this->canBeViewed;
    }

} 