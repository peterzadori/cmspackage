<?php

namespace Packages\CmsPackage\Model\Entities;
use LeanMapper\Fluent;
use LeanMapper\Filtering;

/**
 * Class Container
 * @package Packages\CmsPackage\Model\Entities
 *
 * @property Element[] $elements m:belongsToMany(parent_id)
 */
class Container extends Element implements \IteratorAggregate
{

    /**
     * @return Element[]
     * @throws \LeanMapper\Exception\Exception
     */
    public function getElements()
    {
        $elements = [];
        $rows = $this->getValueByPropertyWithRelationship('elements', new Filtering(function(Fluent $fluent) {
            $fluent->orderBy('[order] ASC');
        }));

        foreach ($rows as $row)
        {
            $elements[$row->order] = $row;
        }

        return $elements;
    }


    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->getElements());
    }

}