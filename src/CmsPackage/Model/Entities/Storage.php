<?php

namespace Packages\CmsPackage\Model\Entities;

class Storage extends Route
{

    protected $canBeViewed = false;
    

    public function getIcon()
    {
        return 'fa-folder-o';
    }

}