<?php

namespace Packages\CmsPackage\Model\Schemas;

use Doctrine\DBAL\Schema\Schema;
use movi\Model\Mapper;
use movi\Model\Schema\ISchemaFactory;
use movi\Model\Schema\Tools\EntitySchemaFactory;
use Packages\CmsPackage\Elements\Section\SectionElementEntity;

class ContentSchemaFactory implements ISchemaFactory
{

    /**
     * @var Mapper
     */
    private $mapper;


    public function __construct(Mapper $mapper)
    {
        $this->mapper = $mapper;
    }


    public function createSchema(Schema $schema)
    {
        $schemaFactory = new EntitySchemaFactory($this->mapper);

        $schemaFactory->createEntitySchema($schema, new SectionElementEntity(), 'elements');
    }

}