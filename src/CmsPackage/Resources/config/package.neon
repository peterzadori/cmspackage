nette:
    latte:
        macros:
            - Packages\CmsPackage\Latte\Macros\CmsMacros
movi:
    entities:
        routes: Packages\CmsPackage\Model\Entities\Route
        categories: Packages\CmsPackage\Model\Entities\Category
        elements: Packages\CmsPackage\Model\Entities\Element

services:
    cms.routesFacade: Packages\CmsPackage\Model\Facades\RoutesFacade(@daoFactory::getDao(Packages\CmsPackage\Model\Entities\Route))
    cms.categoriesFacade: Packages\CmsPackage\Model\Facades\CategoriesFacade(@daoFactory::getDao(Packages\CmsPackage\Model\Entities\Category))
    cms.elementsFacade: Packages\CmsPackage\Model\Facades\ElementsFacade(@daoFactory::getDao(Packages\CmsPackage\Model\Entities\Element))

    # Trees
    cms.routesTree: {class: Packages\CmsPackage\Services\RoutesTree(@cms.routesFacade)}
    cms.categoriesTree: {class: Packages\CmsPackage\Services\CategoriesTree(@cms.categoriesFacade)}

    # Uploaders
    - {class: Packages\CmsPackage\Services\RouteImageUploader}

    - {class: Packages\CmsPackage\Routes\PageType, tags: [cms.route.type]}
    - {class: Packages\CmsPackage\Routes\StorageType, tags: [cms.route.type]}
    - {class: Packages\CmsPackage\Routes\ShortcutType, tags: [cms.route.type]}

    - {class: Packages\CmsPackage\Services\ElementManagement}

    # Routing
    -
        class: movi\Application\Routers\NodeRoute('Cms:Front', 'Page', 'view', NULL, @cms.routesTree)
        tags: [route, order = 50]

    -
        class: movi\Application\Routers\LanguageRoute('sitemap.xml', 'Cms:Front:Sitemap:default')
        tags: [route, order = 51]

    # Form palettes
    - {class: Packages\CmsPackage\Modules\BackModule\Forms\Palettes\HeaderPalette}
    - {class: Packages\CmsPackage\Modules\BackModule\Forms\Palettes\ImagePalette}
    - {class: Packages\CmsPackage\Modules\BackModule\Forms\Palettes\ParentPalette}
    - {class: Packages\CmsPackage\Modules\BackModule\Forms\Palettes\TemplatePalette}
    - {class: Packages\CmsPackage\Modules\BackModule\Forms\Palettes\SettingsPalette}
    - {class: Packages\CmsPackage\Modules\BackModule\Forms\Palettes\CategoriesPalette}

    # Forms
    - {class: Packages\CmsPackage\Modules\BackModule\Forms\PageFormFactory(@cms.routesFacade)}
    - {class: Packages\CmsPackage\Modules\BackModule\Forms\StorageFormFactory(@cms.routesFacade)}
    - {class: Packages\CmsPackage\Modules\BackModule\Forms\ShortcutFormFactory(@cms.routesFacade)}

    - Packages\CmsPackage\Modules\BackModule\Forms\CategoryFormFactory(@cms.categoriesFacade)
    - Packages\CmsPackage\Modules\BackModule\Components\Elements\Forms\ElementVisualFormFactory(@cms.elementsFacade)

    # Presenters
    -
        class: Packages\CmsPackage\Modules\BackModule\Presenters\RoutesPresenter
        tags: [presenter, name = Pages,  group = CMS, group-icon = fa-edit, order = 200]

    -
        class: Packages\CmsPackage\Modules\BackModule\Presenters\CategoriesPresenter
        tags: [presenter, name = Categories, group = CMS, order = 201]

    # Elements
    -
        implement: Packages\CmsPackage\Modules\BackModule\Components\Elements\IElementsControlFactory
        parameters: [route]
        arguments: [%route%]

    -
        implement: Packages\CmsPackage\Modules\BackModule\Components\Blocks\IBlocksControlFactory
        parameters: [route]
        arguments: [%route%]

    -
        implement: Packages\CmsPackage\Modules\BackModule\Components\ElementRenderer\IElementRendererControlFactory
        parameters: [element]
        arguments: [%element%]

    -
        implement: Packages\CmsPackage\Modules\FrontModule\Components\ElementRenderer\IElementRendererControlFactory
        parameters: [element]
        arguments: [%element%]

    -
        implement: Packages\CmsPackage\Modules\FrontModule\Components\ElementsRenderer\IElementsRendererControlFactory

    # Grid Element
    - {implement: Packages\CmsPackage\Elements\Grid\IGridBackendControlFactory}
    - {implement: Packages\CmsPackage\Elements\Grid\IGridFrontendControlFactory}

    -
        class: Packages\CmsPackage\Elements\Grid\GridElement
        tags: [cms.element.type]

    # Latte Element
    - {class: Packages\CmsPackage\Elements\Latte\Forms\LatteFormFactory(@cms.elementsFacade)}
    - {implement: Packages\CmsPackage\Elements\Latte\ILatteElementFrontendControlFactory}
    - {implement: Packages\CmsPackage\Elements\Latte\ILatteElementBackendControlFactory}

    -
        class: Packages\CmsPackage\Elements\Latte\LatteElement
        tags: [cms.element.type]

    # Section Element
    - {class: Packages\CmsPackage\Model\Schemas\ContentSchemaFactory, tags: [schema.factory]}

    - {class: Packages\CmsPackage\Elements\Section\Forms\SectionFormFactory(@cms.elementsFacade)}
    - {implement: Packages\CmsPackage\Elements\Section\ISectionElementBackendControlFactory}
    - {implement: Packages\CmsPackage\Elements\Section\ISectionElementFrontendControlFactory}

    -
        class: Packages\CmsPackage\Elements\Section\SectionElement
        tags: [cms.element.type]

    # Components
    - {implement: Packages\CmsPackage\Modules\BackModule\Components\ElementTypePicker\IElementTypePickerControlFactory}

    # Widgets
    -
        implement: Packages\CmsPackage\Widgets\Navigation\INavigationWidgetFactory
        tags: [widget, name = navigation]

    -
        implement: Packages\CmsPackage\Widgets\Element\IElementWidgetFactory
        tags: [widget, name = element]

    -
        implement: Packages\CmsPackage\Widgets\Sections\ISectionsWidgetFactory
        tags: [widget, name = sections]

    -
        implement: Packages\CmsPackage\Widgets\Breadcrumbs\IBreadcrumbsWidgetFactory
        tags: [widget, name = breadcrumbs]


    # Templating
    cms.routeFilter: Packages\CmsPackage\Latte\Filters\RouteFilter
    cms.wrapFilter: Packages\CmsPackage\Latte\Filters\WrapFilter

    nette.latteFactory:
        setup:
            - addFilter(route, @cms.routeFilter::getRoute)
            - addFilter(wrap, @cms.wrapFilter::wrapControl)
