(function($) {
    // CMS elements
    $.nette.ext('elements', {
        load: function() {
            $('.elements-container').each(function() {
                var $this = $(this);

                var sortable = Sortable.create(this, {
                    animation: 200,
                    group: 'elements',
                    onAdd: function(e) {
                        // Changing parent
                        $.nette.ajax({
                            url: $this.data('saveUrl'),
                            data: {
                                elements: sortable.toArray()
                            },
                            off: ['unique']
                        });
                    },
                    onUpdate: function(e) {
                        // Sorting within parent
                        $.nette.ajax({
                            url: $this.data('saveUrl'),
                            data: {
                                elements: sortable.toArray()
                            },
                            off: ['unique']
                        });
                    }
                });
            });
        }
    });
})(jQuery);