<?php

namespace Packages\CmsPackage\Modules\FrontModule\Presenters;

use movi\Application\UI\Presenter;
use Packages\CmsPackage\Services\RoutesTree;

class SitemapPresenter extends Presenter
{

    /**
     * @var RoutesTree
     */
    private $tree;


    public function __construct(RoutesTree $tree)
    {
        parent::__construct();

        $this->tree = $tree;
    }


    public function renderDefault()
    {
        $this->template->nodes = $this->tree->getNodes();
    }


}