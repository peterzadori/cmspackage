<?php

namespace Packages\CmsPackage\Modules\FrontModule\Presenters;

use Packages\AppPackage\Modules\FrontModule\Presenters\BasePresenter;
use Nette\Application\UI\Multiplier;
use Packages\CmsPackage\Modules\FrontModule\Components\DummyFrontendElement\DummyFrontendElementControl;
use Packages\CmsPackage\Modules\FrontModule\Components\ElementRenderer\IElementRendererControlFactory;
use Packages\CmsPackage\Modules\FrontModule\Components\ElementsRenderer\IElementsRendererControlFactory;

final class PagePresenter extends RoutePresenter
{

	use BasePresenter;


	private $elements;

	/**
	 * @var IElementRendererControlFactory
	 * @inject
	 */
	public $elementRendererFactory;


    /**
     * @var IElementsRendererControlFactory
     * @inject
     */
    public $elementsRendererFactory;


	public function actionView()
	{
		$this->elements = $this->route->elements;

		$this->template->elements = $this->elements;
	}


	public function createTemplate()
	{
		$template = parent::createTemplate();

		if ($this->route->template !== NULL) {
			$templateFile = $this->theme->dir . '/' . $this->route->template;

			if (file_exists($templateFile)) {
				$template->setFile($templateFile);
			}
		}

		return $template;
	}


	protected function createComponentElement()
	{
		return new Multiplier(function($element) {
			return $this->elementRendererFactory->create($element);

		});
	}


    protected function createComponentElements()
    {
        return $this->elementsRendererFactory->create($this->elements);
    }


    protected function createComponentBlocks()
    {
        return new Multiplier(function($block) {
            if (isset($this->route->blocks[$block])) {
                return $this->elementsRendererFactory->create($this->route->blocks[$block]);
            } else {
                return new DummyFrontendElementControl();
            }
        });
    }

} 