<?php

namespace Packages\CmsPackage\Modules\FrontModule\Presenters;

use movi\Application\UI\NodePresenter;
use Packages\CmsPackage\Model\Entities\Route;
use Packages\CmsPackage\Services\RoutesTree;

abstract class RoutePresenter extends NodePresenter
{

	/**
	 * @var RoutesTree
	 */
	protected $routesTree;

    /**
     * @var Route
     */
	public $route;


	public function injectRoutesTree(RoutesTree $routesTree)
	{
		$this->routesTree = $routesTree;
	}


	public function startup()
	{
		parent::startup();

		if ($this->node !== NULL) {
			$this->route = $this->node->entity;
		}

		if (!$this->route->canBeViewed()) {
			$this->error('Page can not be viewed.');
		}
	}


    public function beforeRender()
	{
		parent::beforeRender();

		$this->template->route = $this->route;

        $this['head']->setTitle($this->route->name);

        if (!empty($this->route->meta_keywords)) $this['head']->addMetaTag('keywords', $this->route->meta_keywords);
        if (!empty($this->route->meta_description)) $this['head']->addMetaTag('description', $this->route->meta_description);
	}

} 