<?php

namespace Packages\CmsPackage\Modules\FrontModule\Components\ElementRenderer;

use artem_c\emmet\Emmet;
use movi\Application\UI\Control;
use Nette\Utils\Html;
use Packages\CmsPackage\Content\ElementControl;
use Packages\CmsPackage\Content\ElementTypesManager;
use Packages\CmsPackage\Model\Entities\Element;
use Packages\CmsPackage\Model\Facades\ElementsFacade;

class ElementRendererControl extends Control
{

    /** @var Element */
    private $element;

    /** @var ElementTypesManager */
    private $elementTypesManager;


    public function __construct($element, ElementsFacade $elementsFacade, ElementTypesManager $elementTypesManager)
    {
        $this->elementTypesManager = $elementTypesManager;

        if (!$element instanceof Element) {
            $element = $elementsFacade->findByIdOrAlias($element);
        }

        $this->element = $element;
    }


    public function render()
    {
        if (!$this->element) {
            echo "Element not found";
        } else {
            if ($this->shouldRender()) {
                return parent::render();
            }
        }
    }


    public function beforeRender()
    {
        $this->template->elementWrapper = $this->getElementWrapper();
        $this->template->element = $this->element;
        $this->template->type = $this->elementTypesManager->getType($this->element->type);
    }


    protected function createComponentElement()
    {
        $type = $this->elementTypesManager->getType($this->element->type);

        /** @var ElementControl $control */
        $control = $type->getFrontendControl();
        $control->setElement($this->element);

        return $control;
    }


    public function getElementWrapper()
    {
        $el = Html::el('div');
        $el->id('c' . $this->element->id);
        $el->class[] = 'content-element';
        $el->class[] = $this->element->custom_class;
        $el->class[] = 'content-element-' . $this->element->type;

        $style = [];
        if ($this->element->margin_top) $style[] = sprintf('margin-top: %dpx', $this->element->margin_top);
        if ($this->element->margin_bottom) $style[] = sprintf('margin-bottom: %dpx', $this->element->margin_bottom);

        if (!empty($style)) {
            $el->style(implode('; ', $style));
        }

        return $el;
    }


    private function shouldRender()
    {
        if (!$this->element->hidden_time) return true;

        $days = explode('|', $this->element->hidden_time);
        $toCheck = [];
        $ranges = [];

        foreach ($days as $day)
        {
            list($day, $start, $end) = array_pad(explode('-', $day, 3), 3, NULL);

            $toCheck[$day] = [(int) $day, $start, $end];
        }

        ksort($toCheck);

        $start = false;
        $previousEnded = true;

        // Compute missing dates
        for ($i = 1; $i <= 7; $i++) {
            if (isset($toCheck[$i])) {
                $day = $toCheck[$i];

                if (!$start) {
                    $start = true;
                }

                if (!$day[2]) {
                    $previousEnded = false;
                } else {
                    $previousEnded = true;
                }

                $ranges[] = $toCheck[$i];
            } else if ($start && !$previousEnded) {
                $ranges[] = [$i, NULL, NULL];
            }
        }

        $days = [1 => 'monday', 2 => 'tuesday', 3 => 'wednesday', 4 => 'thursday', 5 => 'friday', 6 => 'saturday', 7 => 'sunday'];
        $timestamp = time();

        foreach ($ranges as $range)
        {
            list($day, $start, $end) = $range;

            if (!$start) $start = '00:00:00';
            if (!$end) $end = '23:59:59';

            $day = $days[$day];
            $start = strtotime("this week $day $start");
            $end = strtotime("this week $day $end");

            if ($timestamp >= $start && $timestamp <= $end) {
                return false;
            }
        }

        return true;
    }

}