<?php

namespace Packages\CmsPackage\Modules\FrontModule\Components\ElementRenderer;

interface IElementRendererControlFactory
{

    /**
     * @param $element
     * @return ElementRendererControl
     */
    public function create($element);

}