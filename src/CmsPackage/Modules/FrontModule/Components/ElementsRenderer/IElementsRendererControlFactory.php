<?php

namespace Packages\CmsPackage\Modules\FrontModule\Components\ElementsRenderer;

interface IElementsRendererControlFactory
{

    /**
     * @param array $elements
     * @return ElementsRendererControl
     */
    public function create(array $elements);

}