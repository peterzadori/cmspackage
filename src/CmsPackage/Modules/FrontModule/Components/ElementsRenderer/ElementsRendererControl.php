<?php

namespace Packages\CmsPackage\Modules\FrontModule\Components\ElementsRenderer;

use movi\Application\UI\Control;
use Packages\CmsPackage\Content\ElementControl;
use Packages\CmsPackage\Content\ElementTypesManager;
use Packages\CmsPackage\Model\Entities\Element;
use Packages\CmsPackage\Model\Facades\ElementsFacade;
use Packages\CmsPackage\Modules\FrontModule\Components\ElementRenderer\IElementRendererControlFactory;

class ElementsRendererControl extends Control
{

    /**
     * @var Element[]
     */
    private $elements;

    /**
     * @var IElementRendererControlFactory
     */
    private $elementRendererControlFactory;


    /**
     * ElementsRendererControl constructor.
     * @param array $elements
     * @param IElementRendererControlFactory $elementRendererControlFactory
     */
    public function __construct(array $elements, IElementRendererControlFactory $elementRendererControlFactory)
    {
        $this->elements = $elements;
        $this->elementRendererControlFactory = $elementRendererControlFactory;
    }


    public function beforeRender()
    {
        $this->template->elements = $this->elements;
    }


    protected function createComponent($index)
    {
        $element = $this->elements[$index];

        return $this->elementRendererControlFactory->create($element);
    }

}