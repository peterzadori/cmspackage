<?php

namespace Packages\CmsPackage\Modules\BackModule\Presenters;

use movi\Application\UI\Form;
use movi\Components\Tree\TreeControl;
use movi\Tree\Node;
use Packages\AppPackage\Modules\BackModule\Presenters\EntityPresenter;
use Packages\CmsPackage\Model\Entities\Category;
use Packages\CmsPackage\Model\Facades\CategoriesFacade;
use Packages\CmsPackage\Modules\BackModule\Forms\CategoryFormFactory;
use Packages\CmsPackage\Services\CategoriesTree;

/**
 * Class CategoriesPresenter
 * @package Packages\CmsPackage\Modules\BackModule\Presenters
 *
 * @entity Category
 */
class CategoriesPresenter extends EntityPresenter
{

    /**
     * @var CategoriesTree
     */
    private $categoriesTree;

    /**
     * @var CategoryFormFactory
     * @inject
     */
    public $categoryFormFactory;


    public function __construct(CategoriesTree $categoriesTree, CategoriesFacade $categoriesFacade)
    {
        parent::__construct();

        $this->categoriesTree = $categoriesTree;
        $this->facade = $categoriesFacade;
    }


    public function handleDelete($id)
    {
        $this->categoriesTree->build();

        $node = $this->categoriesTree->getNode($id);

        $this->categoriesTree->removeNode($node);

        $this->forward('default');
    }


    public function beforeRender()
    {
        parent::beforeRender();

        $this->redrawControl('content');
    }


    public function renderDefault()
    {
        $this['tree']->redrawControl();
    }


    /**
     * @return \movi\Application\UI\Form
     */
    protected function createComponentForm()
    {
        $form = $this->categoryFormFactory->create($this->entity);
        $form->elementPrototype->class[] = 'ajax';

        $form->onSuccess[] = function(Form $form) {
            $this->flashMessage('Category was saved.');

            if ($this->action === 'add') $form->setValues([], true);

            $this['tree']->redrawControl();
        };

        return $form;
    }


    /**
     * @return TreeControl
     */
    protected function createComponentTree()
    {
        $control = new TreeControl($this->categoriesTree);
        $control->buttonAdd = 'Add Category';
        $control->noNodesNotice = 'No categories have been created. Begin by clicking on Add Category.';

        $control->onAdd[] = function() {
            $this->forward('add');
        };

        $control->onSelect[] = function(Node $node) {
            $this->forward('edit', ['id' => $node->id]);
        };

        return $control;
    }

}