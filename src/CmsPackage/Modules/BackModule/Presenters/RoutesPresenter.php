<?php

namespace Packages\CmsPackage\Modules\BackModule\Presenters;

use Packages\AppPackage\Modules\BackModule\Presenters\EntityPresenter;
use Packages\CmsPackage\Content\RouteTypesManager;
use Packages\CmsPackage\Model\Facades\RoutesFacade;
use Packages\CmsPackage\Modules\BackModule\Components\Elements\IElementsControlFactory;
use Packages\CmsPackage\Model\Entities\Route;
use Packages\CmsPackage\Modules\BackModule\Tools\RouteFormFactoryClassResolver;
use Packages\CmsPackage\Services\RoutesTree;
use movi\Components\Tree\TreeControl;
use movi\Tree\Node;

/**
 * Class RoutesPresenter
 * @package Packages\CmsPackage\Modules\BackModule\Presenters
 *
 * @entity Route
 */
final class RoutesPresenter extends EntityPresenter
{

    /**
     * @persistent
     */
    public $id;

    /**
     * @var RoutesTree
     */
    private $routesTree;

    /**
     * @var RouteTypesManager
     */
    private $routeTypesManager;

    /**
     * @var IElementsControlFactory
     * @inject
     */
    public $elementsControlFactory;

    /**
     * @persistent
     */
    public $routeType;


    public function __construct(RoutesTree $routesTree, RoutesFacade $routesFacade, RouteTypesManager $routeTypesManager)
    {
        parent::__construct();

        $this->facade = $routesFacade;
        $this->routesTree = $routesTree;
        $this->routeTypesManager = $routeTypesManager;
    }


    public function startup()
    {
        parent::startup();

        if ($this->isAjax()) {
            $this->redrawControl('content');
        }
    }


    public function actionDefault()
    {
        $this['tree']->redrawControl();
    }


    public function actionAdd()
    {
        $routeType = $this->routeTypesManager->getType($this->routeType);
        $this->entity = $routeType->getEntity();
        $this->entity->entity = get_class($this->entity);
    }


    public function actionElements($id, $redraw = false)
    {
        $route = $this->facade->findOne($id);

        $this->entity = $route;

        $this->template->route = $route;
        $this->template->elements = $route->elements;
    }


    public function renderElements($id, $redraw = false)
    {
        if ($redraw) $this['tree']->redrawControl();
    }


    public function actionDelete($node)
    {
        $this->routesTree->build();

        $node = $this->routesTree->getNode($node);

        $this->routesTree->removeNode($node);

        $this->flashMessage('Page was removed.');
        $this->forward('default');
    }


    public function handleMarkAsHomepage()
    {
        $current = $this->routesTree->getHomepage();

        if ($current !== NULL) {
            $current->entity->homepage = false;
            $this->facade->persist($current->entity);
        }

        $this->entity->homepage = true;

        $this->facade->persist($this->entity);

        $this->flashMessage('Page was marked as the homapge page.');
    }


    /**
     * @return \movi\Application\UI\Form
     */
    protected function createComponentForm()
    {
        $formFactoryClass = RouteFormFactoryClassResolver::resolveClassName($this->entity);
        $formFactory = $this->context->getByType($formFactoryClass);

        $form = $formFactory->create($this->entity);
        $form->elementPrototype->class[] = 'ajax';

        $form->onSuccess[] = function() {
            $this->flashMessage('Page was saved.');

            if ($this->action === 'add') {
                $this->forward('elements', $this->entity->id, true);
            } else {
                $this['tree']->redrawControl();
            }
        };

        return $form;
    }


    /**
     * @return \Packages\CmsPackage\Modules\BackModule\Components\Elements\ElementsControl
     */
    protected function createComponentElements()
    {
        return $this->elementsControlFactory->create($this->entity);
    }


    protected function createComponentTree()
    {
        $control = new TreeControl($this->routesTree);
        $control->buttonAdd = 'Add Page';
        $control->noNodesNotice = 'No pages have been created. Begin by clicking on Add Page.';
        $control->dropdownHeader = 'Select page type';

        foreach ($this->routeTypesManager->getTypes() as $type)
        {
            $control->addNodeType($type->getKey(), $type->getLabel());
        }

        $control->onAdd[] = function($type) {
            $this->routeType = $type;

            $this->forward('add');
        };

        $control->onSelect[] = function(Node $node) {
            $this->forward('elements', ['id' => $node->id]);
        };

        $control->onMove[] = function() use ($control) {
            $this->redrawControl(NULL, false);
            $control->redrawControl(NULL, false);
        };

        return $control;
    }

} 