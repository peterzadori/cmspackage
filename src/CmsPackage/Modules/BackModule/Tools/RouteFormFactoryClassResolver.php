<?php

namespace Packages\CmsPackage\Modules\BackModule\Tools;

use Doctrine\Common\Inflector\Inflector;
use Packages\CmsPackage\Model\Entities\Route;

class RouteFormFactoryClassResolver
{

    static public function resolveClassName(Route $entity)
    {
        $parts = explode('\\', get_class($entity));

        return sprintf('Packages\\%s\\Modules\\BackModule\\Forms\\%sFormFactory', $parts['1'], $parts['4']);
    }

}