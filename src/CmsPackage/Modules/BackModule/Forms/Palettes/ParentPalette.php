<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms\Palettes;

use movi\Application\UI\Form;
use movi\Forms\AbstractPalette;
use movi\Forms\Controls\TreeControl;
use Packages\CmsPackage\Services\RoutesTree;

class ParentPalette extends AbstractPalette
{

    /**
     * @var RoutesTree
     */
    private $routesTree;


    public function __construct(RoutesTree $routesTree)
    {
        $this->routesTree = $routesTree;
    }


    public function configure(Form $form)
    {
        $route = $this->formFactory->getEntity();

        $form->addGroup('Position');

        $form['parent'] = (new TreeControl('Parent Page'))
            ->setTree($this->routesTree, !$route->isDetached() ? $route->id : NULL)
            ->setPrompt('Vyberte');
    }

}