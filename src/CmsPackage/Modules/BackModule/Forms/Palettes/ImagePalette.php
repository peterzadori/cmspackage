<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms\Palettes;

use movi\Application\UI\Form;
use movi\Forms\AbstractPalette;
use movi\Forms\Controls\ImageControl;
use Packages\CmsPackage\Services\RouteImageUploader;

class ImagePalette extends AbstractPalette
{

    /**
     * @var RouteImageUploader
     */
    private $routeImageUploader;


    public function __construct(RouteImageUploader $routeImageUploader)
    {
        $this->routeImageUploader = $routeImageUploader;
    }


    public function configure(Form $form)
    {
        $entity = $this->formFactory->getEntity();
        $facade = $this->formFactory->getFacade();

        $form->addGroup('Media');

        $image = new ImageControl('Image', $this->routeImageUploader);
        $image->onDelete[] = function() use ($entity, $facade) {
            $entity->image = NULL;

            $facade->persist($entity);
        };
        $form['image'] = $image;
    }

}