<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms\Palettes;

use movi\Application\UI\Form;
use movi\Forms\AbstractPalette;
use movi\Forms\Controls\HasMany;
use Packages\CmsPackage\Services\CategoriesTree;

class CategoriesPalette extends AbstractPalette
{

    /**
     * @var CategoriesTree
     */
    private $categoriesTree;


    public function __construct(CategoriesTree $categoriesTree)
    {
        $this->categoriesTree = $categoriesTree;
    }


    public function configure(Form $form)
    {
        $form['categories'] = (new HasMany('Categories'))
            ->setTree($this->categoriesTree);
    }

}