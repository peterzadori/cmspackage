<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms\Palettes;

use movi\Application\UI\Form;
use movi\Forms\AbstractPalette;

class SettingsPalette extends AbstractPalette
{

    public function configure(Form $form)
    {
        $form->addGroup('Page Settings');

        $form->addCheckbox('hidden', 'Hidden in navigations');

        $form->addText('alias', 'Alias')
            ->setOption('description', 'Alias is used in templates.');
    }

}