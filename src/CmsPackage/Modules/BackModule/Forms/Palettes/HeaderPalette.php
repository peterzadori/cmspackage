<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms\Palettes;

use movi\Application\UI\Form;
use movi\Forms\AbstractPalette;
use movi\Forms\Controls\Translations;
use Nette\Forms\Container;

class HeaderPalette extends AbstractPalette
{

    public function configure(Form $form)
    {
        $form['translations'] = new Translations(function(Container $container) {
            $container->addText('name', 'Page Name')
                ->setRequired();

            $container->addText('title', 'Page Title');

            $container->addTextArea('abstract', 'Abstract');
        });
    }

}