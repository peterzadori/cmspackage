<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms\Palettes;

use movi\Application\UI\Form;
use movi\Forms\AbstractPalette;

class MetaPalette extends AbstractPalette
{

    public function configure(Form $form)
    {
        $form->addGroup('META information');

        $form->addText('meta_keywords', 'Keywords');

        $form->addText('meta_description', 'Description');
    }

}