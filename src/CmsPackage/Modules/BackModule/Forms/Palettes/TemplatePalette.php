<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms\Palettes;

use movi\Application\UI\Form;
use movi\Caching\CacheProvider;
use movi\Forms\AbstractPalette;
use movi\Utils\Strings;
use Nette\Utils\Finder;
use Packages\AppPackage\Themes\Theme;

class TemplatePalette extends AbstractPalette
{

    /**
     * @var Theme
     */
    private $theme;

    /**
     * @var \Nette\Caching\Cache
     */
    private $cache;


    public function __construct(Theme $theme, CacheProvider $cacheProvider)
    {
        $this->theme = $theme;
        $this->cache = $cacheProvider->create('route.templates');
    }


    public function configure(Form $form)
    {
        if (!empty($this->theme->dir)) {
            $form->addGroup('Visual');

            $form->addSelect('template', 'Custom Template', $this->getTemplates())
                ->setPrompt('Vyberte');
        }
    }


    private function getTemplates()
    {
        if (!$this->cache->load('templates')) {
            $templates = array();

            foreach (Finder::find('*.latte')->from($this->theme->dir . '/templates') as $file)
            {
                $template = Strings::substring($file->getRealPath(), strlen($this->theme->dir) + 1);
                $templates[$template] = $template;
            }

            $this->cache->save('templates', $templates);
        }

        return $this->cache->load('templates');
    }

}