<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms;

use movi\Application\UI\Form;

class PageFormFactory extends AbstractRouteFormFactory
{

    protected function configure(Form $form)
    {
        $this->getPalette('header')->attach($form);

        $this->getPalette('parent')->attach($form);

        $this->getPalette('image')->attach($form);

        $this->getPalette('meta')->attach($form);

        $this->getPalette('template')->attach($form);

        $this->getPalette('settings')->attach($form);

        $this->getPalette('categories')->attach($form);
    }

} 