<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms;

use movi\Application\UI\Form;

class StorageFormFactory extends AbstractRouteFormFactory
{

    protected function configure(Form $form)
    {
        $this->getPalette('header')->configure($form);

        $this->getPalette('parent')->configure($form);
    }


    public function preProcessForm(Form $form)
    {
        parent::preProcessForm($form);

        $this->entity->hidden = true;
    }

} 