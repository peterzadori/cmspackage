<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms;

use movi\Application\UI\Form;
use movi\Forms\TreeFormFactory;
use Packages\CmsPackage\Modules\BackModule\Forms\Palettes\CategoriesPalette;
use Packages\CmsPackage\Modules\BackModule\Forms\Palettes\HeaderPalette;
use Packages\CmsPackage\Modules\BackModule\Forms\Palettes\ImagePalette;
use Packages\CmsPackage\Modules\BackModule\Forms\Palettes\MetaPalette;
use Packages\CmsPackage\Modules\BackModule\Forms\Palettes\ParentPalette;
use Packages\CmsPackage\Modules\BackModule\Forms\Palettes\SettingsPalette;
use Packages\CmsPackage\Modules\BackModule\Forms\Palettes\TemplatePalette;
use Packages\CmsPackage\Services\RoutesTree;

abstract class AbstractRouteFormFactory extends TreeFormFactory
{

    /**
     * @var RoutesTree
     * @inject
     */
    public $tree;

    /**
     * @var HeaderPalette
     * @inject
     */
    public $headerPalette;

    /**
     * @var ImagePalette
     * @inject
     */
    public $imagePalette;

    /**
     * @var ParentPalette
     * @inject
     */
    public $parentPalette;

    /**
     * @var TemplatePalette
     * @inject
     */
    public $templatePalette;

    /**
     * @var SettingsPalette
     * @inject
     */
    public $settingsPalette;

    /**
     * @var CategoriesPalette
     * @inject
     */
    public $categoriesPalette;


    protected function preConfigure(Form $form)
    {
        $this->addPalette('header', $this->headerPalette);
        $this->addPalette('image', $this->imagePalette);
        $this->addPalette('parent', $this->parentPalette);
        $this->addPalette('template', $this->templatePalette);
        $this->addPalette('settings', $this->settingsPalette);
        $this->addPalette('categories', $this->categoriesPalette);
        $this->addPalette('meta', new MetaPalette());
    }


    protected function postConfigure(Form $form)
    {
        $form->addSubmit('save', 'Save');
    }


    public function preProcessForm(Form $form)
    {
        $this->entity->modified = new \DateTime();
    }

}