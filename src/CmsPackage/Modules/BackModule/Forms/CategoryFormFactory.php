<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms;

use movi\Application\UI\Form;
use movi\Forms\Controls\TreeControl;
use movi\Forms\Renderer;
use movi\Forms\TranslationsFormFactory;
use movi\Forms\Controls\Translations;
use movi\Forms\TreeFormFactory;
use movi\Tree\Node;
use Nette\Forms\Container;
use Packages\CmsPackage\Services\RouteNode;
use Packages\CmsPackage\Services\CategoriesTree;

class CategoryFormFactory extends TreeFormFactory
{

	/**
	 * @var CategoriesTree
	 * @inject
	 */
	public $tree;


	protected function configure(Form $form)
	{
		$form['translations'] = new Translations(function(Container $container) {
			$container->addText('name', 'Category Name')
				->setRequired();
		});

		$form->addGroup('Position')->setOption('order', 90);
		$form['parent'] = (new TreeControl('Category Parent'))
			->setTree($this->tree, !$this->entity->isDetached() ? $this->entity->id : NULL)
			->setPrompt('Select');

		$form->addSubmit('save', 'Save')->setOption('icon', 'fa fa-check');
	}

} 