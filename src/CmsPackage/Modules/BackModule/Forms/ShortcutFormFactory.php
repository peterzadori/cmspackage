<?php

namespace Packages\CmsPackage\Modules\BackModule\Forms;

use movi\Application\UI\Form;
use movi\Forms\Controls\TreeControl;

class ShortcutFormFactory extends AbstractRouteFormFactory
{

    protected function configure(Form $form)
    {
        $this->getPalette('header')->configure($form);

        $this->getPalette('parent')->configure($form);

        $form->addGroup('Shortcut');

        $form['shortcut'] = (new TreeControl('Target Page'))
            ->setTree($this->tree)
            ->setPrompt('Select');

        $this->getPalette('settings')->configure($form);
    }

} 