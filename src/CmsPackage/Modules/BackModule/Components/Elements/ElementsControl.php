<?php

namespace Packages\CmsPackage\Modules\BackModule\Components\Elements;

use movi\Application\UI\Control;
use movi\Application\UI\Form;
use Nette\Application\UI\Multiplier;
use Packages\CmsPackage\Content\ElementType;
use Packages\CmsPackage\Content\ElementTypesManager;
use Packages\CmsPackage\Model\Entities\Element;
use Packages\CmsPackage\Model\Entities\ReorderElements;
use Packages\CmsPackage\Model\Entities\Route;
use Packages\CmsPackage\Model\Facades\ElementsFacade;
use Packages\CmsPackage\Modules\BackModule\Components\Blocks\IBlocksControlFactory;
use Packages\CmsPackage\Modules\BackModule\Components\ElementRenderer\IElementRendererControlFactory;
use Packages\CmsPackage\Modules\BackModule\Components\Elements\Forms\ElementSettingsFormFactory;
use Packages\CmsPackage\Modules\BackModule\Components\Elements\Forms\ElementVisualFormFactory;
use Packages\CmsPackage\Modules\BackModule\Components\ElementTypePicker\IElementTypePickerControlFactory;
use Packages\CmsPackage\Services\ElementManagement;
use Packages\CmsPackage\Trees\ElementsTree;

class ElementsControl extends Control
{

    /**
     * @var Route
     */
    private $route;

    /**
     * @var ElementTypesManager
     */
    private $typesManager;

    /**
     * @var ElementManagement
     */
    private $elementManagement;

    /**
     * @var ElementsFacade
     */
    private $elementsFacade;

    private $elementsTree;

    /**
     * @var IElementTypePickerControlFactory
     * @inject
     */
    public $typePickerControlFactory;

    /**
     * @var ElementVisualFormFactory
     * @inject
     */
    public $elementVisualFormFactory;

    /**
     * @var IBlocksControlFactory
     * @inject
     */
    public $blocksControlFactory;

    /**
     * @persistent
     */
    public $customize = false;

    /**
     * @persistent
     */
    public $visual = false;

    /**
     * @persistent
     */
    public $element;

    /**
     * @persistent
     */
    public $parent;

    /**
     * @var Element
     */
    private $entity;


    public function __construct(Route $route, ElementsFacade $elementsFacade, ElementManagement $elementManagement, ElementTypesManager $typesManager)
    {
        $this->route = $route;
        $this->elementsFacade = $elementsFacade;
        $this->elementManagement = $elementManagement;
        $this->typesManager = $typesManager;
    }


    public function handleDelete()
    {
        $element = $this->elementsFacade->findOne($this->element);
        $this->elementManagement->deleteElement($element);

        $this->element = NULL;

        $this->presenter->flashMessage('Element was removed.');
        $this->redrawControl();
    }


    /**
     * @return \Packages\CmsPackage\Content\ElementControl
     * @throws \movi\InvalidArgumentException
     */
    protected function createComponentElement()
    {
        $element = $this->elementsFacade->findOne($this->element);
        $type = $this->typesManager->getType($element->type);

        $control = $type->getBackendControl();
        $control->setElement($element);

        return $control;
    }


    /**
     * @return Form
     */
    protected function createComponentCustomize()
    {
        $factory = new ElementSettingsFormFactory($this->getCurrentElementType()->getElementSettingsFactory(), $this->presenter->context);
        $form = $factory->create($this->entity);

        $form->onSuccess[] = function(Form $form) {
            $values = $form->getValues(true);

            $this->entity->setSettings($values);

            $this->elementsFacade->persist($this->entity);

            $this->presenter->flashMessage('Settings were saved.');

            $this->customize = false;
            $this->redrawControl();
        };

        return $form;
    }


    protected function createComponentVisual()
    {
        $form = $this->elementVisualFormFactory->create($this->entity);
        $form->elementPrototype->class[] = 'ajax';

        $form->onSuccess[] = function() {
            $this->presenter->flashMessage('Settings were saved.');

            $this->visual = false;
            $this->redrawControl();
        };

        return $form;
    }


    protected function createComponentBlocks()
    {
        return $this->blocksControlFactory->create($this->route);
    }


    public function beforeRender()
    {
        $template = $this->template;

        $template->route = $this->route;
        $template->elements = $this->route->elements;
        $template->types = $this->typesManager->getTypes();

        if ($this->element) {
            $template->element = $this->entity;
            $template->type = $this->getCurrentElementType();
        }
    }


    public function loadState(array $params)
    {
        parent::loadState($params);

        if ($this->element) {
            $this->entity = $this->elementsFacade->findOne($this->element);
        }
    }


    /**
     * Helpers
     */

    /**
     * @param Element $element
     * @return ElementType
     * @throws \movi\InvalidArgumentException
     */
    public function getElementType(Element $element)
    {
        return $this->typesManager->getType($element->type);
    }


    /**
     * @return ElementType
     */
    public function getCurrentElementType()
    {
        return $this->getElementType($this->entity);
    }


    public function setElement(Element $element)
    {
        $this->entity = $element;

        if ($this->getCurrentElementType()->isEditable()) {
            $this->element = $element->id;
        }
    }


    public function getRoute()
    {
        return $this->route;
    }

}