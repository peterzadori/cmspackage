<?php

namespace Packages\CmsPackage\Modules\BackModule\Components\Elements\Forms;

use movi\Application\UI\Form;
use movi\Forms\EntityFormFactory;
use movi\Utils\Strings;
use Nette\Forms\IControl;
use Nette\Utils\Html;

class ElementVisualFormFactory extends EntityFormFactory
{
    protected function configure(Form $form)
    {
        $form->addText('name', 'Name')
            ->setOption('description', 'Name is visible only in back-end.');

        $form->addText('custom_class', 'Custom CSS Class');

        $form->addText('alias', 'Alias')->setOption('description', 'Alias is used in templates.');

        $form->addText('margin_top', 'Top Margin');

        $form->addText('margin_bottom', 'Bottom Margin');

        $form->addTextArea('wrapper_html', 'HTML Wrapper around element')->setOption('description', Html::el()->setHtml('You can use emmet-like syntax. Do not forget to add <code>{`output`}</code>.'))->addCondition($form::FILLED)->addRule(function (
                IControl $control
            ) {
                $value = $control->getValue();

                return Strings::contains($value, '{`output`}');
            }, 'Missing {`output`}.');

        $form->addText('hidden_time', 'Hidden when')->setOption('description', Html::el()->setHtml('Format: <code>DAY<small>(1-7)</small>-<b>FROM</b><small>(HH:MM)</small>-<b>TO</b><small>(HH:MM)</small>|DAY2...</code>'));

        $form->addSubmit('save', 'Save');
    }
}