<?php

namespace Packages\CmsPackage\Modules\BackModule\Components\Elements\Forms;

use movi\Application\UI\Form;
use movi\Forms\FormFactory;
use Nette\DI\Container;
use Packages\CmsPackage\Content\ElementSettings;
use Packages\CmsPackage\Model\Entities\Element;

class ElementSettingsFormFactory extends FormFactory
{

    private $elementSettings;

    /**
     * @var Container
     */
    private $serviceLocator;

    /** @var Element */
    private $element;


    public function __construct(ElementSettings $elementSettings, Container $serviceLocator)
    {
        $this->elementSettings = $elementSettings;
        $this->serviceLocator = $serviceLocator;
    }


    public function create(Element $element = NULL)
    {
        $this->element = $element;

        return parent::create();
    }


    public function configure(Form $form)
    {
        $form->elementPrototype->class[] = 'ajax';

        $this->serviceLocator->callInjects($this->elementSettings);
        $this->elementSettings->configure($form);

        $form->addGroup();

        $form->addSubmit('save', 'Save');
    }


    public function loadValues(Form $form)
    {
        $settings = $this->element->getSettings(true);

        $form->setDefaults($settings);
    }

}