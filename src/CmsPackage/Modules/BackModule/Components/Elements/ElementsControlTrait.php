<?php

namespace Packages\CmsPackage\Modules\BackModule\Components\Elements;

trait ElementsControlTrait
{

    /**
     * @return ElementsControl
     */
    public function getElementsControl()
    {
        return $this->lookup(ElementsControl::class);
    }

}