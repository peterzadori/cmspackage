<?php

namespace Packages\CmsPackage\Modules\BackModule\Components\Elements;

interface IElementsControlFactory
{

    /**
     * @return ElementsControl
     */
    public function create($route);

}