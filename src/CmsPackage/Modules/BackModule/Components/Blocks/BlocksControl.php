<?php

namespace Packages\CmsPackage\Modules\BackModule\Components\Blocks;

use movi\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nette\InvalidStateException;
use Packages\AppPackage\Themes\Theme;
use Packages\CmsPackage\Content\ElementType;
use Packages\CmsPackage\Content\RouteTypesManager;
use Packages\CmsPackage\Model\Entities\Element;
use Packages\CmsPackage\Model\Entities\Route;
use Packages\CmsPackage\Modules\BackModule\Components\ElementRenderer\IElementRendererControlFactory;
use Packages\CmsPackage\Modules\BackModule\Components\ElementTypePicker\IElementTypePickerControlFactory;
use Packages\CmsPackage\Services\ElementManagement;

class BlocksControl extends Control
{

    /**
     * @var Route
     */
    private $route;

    /**
     * @var Theme
     */
    private $theme;

    /**
     * @var RouteTypesManager
     */
    private $routeTypesManager;

    /**
     * @var ElementManagement
     */
    private $elementManagement;

    /**
     * @var IElementTypePickerControlFactory
     * @inject
     */
    public $elementTypePickerFactory;

    /**
     * @var IElementRendererControlFactory
     * @inject
     */
    public $elementRendererControlFactory;


    public function __construct(Route $route, Theme $theme, RouteTypesManager $routeTypesManager, ElementManagement $elementManagement)
    {
        $this->route = $route;
        $this->theme = $theme;
        $this->routeTypesManager = $routeTypesManager;
        $this->elementManagement = $elementManagement;
    }


    public function beforeRender()
    {
        $type = $this->routeTypesManager->getTypeByEntity(get_class($this->route));

        if (!$type) {
            throw new InvalidStateException('Unknown route type: ' . get_class($this->route));
        }

        $key = $type->getKey();
        $blocks = [];

        if (isset($this->theme->blocks->{$key})) {
            $blocks = $this->theme->blocks->{$key};
        }

        $this->template->blocks = $blocks;
        $this->template->route = $this->route;
    }


    public function handleSaveOrder()
    {
        $elements = $this->presenter->getHttpRequest()->getQuery('elements');
        $this->elementManagement->saveElementsOrder($elements, $this->route, NULL);

        $this->presenter->flashMessage('Order was saved.');
    }


    protected function createComponentBlockElementTypePicker()
    {
        return new Multiplier(function($block) {
            $control = $this->elementTypePickerFactory->create($block);

            $control->onAdd[] = function(ElementType $elementType, $block) {
                $element = $this->elementManagement->addElement($elementType, $this->route, null, null, $block);

                $this->parent->setElement($element);
                $this->parent->redrawControl();
            };

            return $control;
        });
    }


    protected function createComponentRouteElementTypePicker()
    {
        $control = $this->elementTypePickerFactory->create();

        $control->onAdd[] = function(ElementType $elementType) {
            $element = $this->elementManagement->addElement($elementType, $this->route);

            $this->parent->setElement($element);
            $this->parent->redrawControl();
        };

        return $control;
    }


    /**
     * @return Multiplier
     */
    protected function createComponentElementRenderer()
    {
        return new Multiplier(function($element) {
            // Render element using element renderer
            $renderer = $this->elementRendererControlFactory->create($element);

            $renderer->onDelete[] = function(Element $element) {
                $this->elementManagement->deleteElement($element);

                $this->parent->element = NULL;

                $this->presenter->flashMessage('Element was removed.');
                $this->redrawControl();
            };

            return $renderer;
        });
    }

}