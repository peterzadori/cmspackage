<?php

namespace Packages\CmsPackage\Modules\BackModule\Components\Blocks;

interface IBlocksControlFactory
{

    /**
     * @param $route
     *
     * @return BlocksControl
     */
    public function create($route);

}