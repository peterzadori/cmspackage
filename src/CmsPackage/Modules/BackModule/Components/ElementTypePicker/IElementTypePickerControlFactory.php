<?php

namespace Packages\CmsPackage\Modules\BackModule\Components\ElementTypePicker;

interface IElementTypePickerControlFactory
{

    /**
     * @return ElementTypePickerControl
     */
    public function create($block = NULL);

}