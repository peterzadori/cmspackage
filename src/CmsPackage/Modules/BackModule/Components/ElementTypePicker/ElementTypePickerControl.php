<?php

namespace Packages\CmsPackage\Modules\BackModule\Components\ElementTypePicker;

use movi\Application\UI\Control;
use Packages\CmsPackage\Content\ElementTypesManager;

class ElementTypePickerControl extends Control
{

    /**
     * @var string
     */
    private $block;

    /**
     * @var ElementTypesManager
     * @inject
     */
    public $elementTypesManager;


    public $onAdd;


    public function __construct($block = NULL)
    {
        $this->block = $block;
    }


    public function handleAdd($type)
    {
        $elementType = $this->elementTypesManager->getType($type);

        $this->onAdd($elementType, $this->block);
    }


    public function beforeRender()
    {
        $this->template->types = $this->elementTypesManager->getTypes();
    }

}