<?php

namespace Packages\CmsPackage\Modules\BackModule\Components\ElementRenderer;

use movi\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Packages\CmsPackage\Content\ElementControl;
use Packages\CmsPackage\Content\ElementTypesManager;
use Packages\CmsPackage\Model\Entities\Element;
use Packages\CmsPackage\Model\Facades\ElementsFacade;
use Packages\CmsPackage\Modules\BackModule\Components\Elements\ElementsControlTrait;

class ElementRendererControl extends Control
{

    use ElementsControlTrait;


    /** @var Element */
    private $element;

    /** @var ElementTypesManager */
    private $elementTypesManager;

    private $preview = true;


    public $onDelete;


    public function __construct($element, ElementsFacade $elementsFacade, ElementTypesManager $elementTypesManager)
    {
        $this->elementTypesManager = $elementTypesManager;

        if (!$element instanceof Element) {
            $element = $elementsFacade->findOne($element);
        }

        $this->element = $element;
    }


    public function handleDelete()
    {
        $this->onDelete($this->element);
    }


    public function beforeRender()
    {
        $this->template->element = $this->element;
        $this->template->type = $this->elementTypesManager->getType($this->element->type);
        $this->template->preview = $this->preview;
    }


    protected function createComponentElement()
    {
        $type = $this->elementTypesManager->getType($this->element->type);

        /** @var ElementControl $control */
        $control = $type->getBackendControl();
        $control->setElement($this->element);

        return $control;
    }


    public function setPreview($preview)
    {
        $this->preview = $preview;

        return $this;
    }

}