<?php

namespace Packages\CmsPackage\Modules\BackModule\Components\ElementRenderer;

interface IElementRendererControlFactory
{

    /**
     * @return ElementRendererControl
     */
    public function create($element);

}