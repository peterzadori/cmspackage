<?php

namespace Packages\CmsPackage\Elements\Latte;

use Latte\Loaders\StringLoader;
use Nette\Bridges\ApplicationLatte\Template;
use Packages\CmsPackage\Content\ElementControl;

class LatteElementFrontendControl extends ElementControl
{

    public function render()
    {
        /** @var Template $template */
        $template = $this->presenter->getTemplate();
        $template->getLatte()->setLoader(new StringLoader());

        $template->setFile($this->element->content);

        $template->render();
    }

}