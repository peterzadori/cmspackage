<?php

namespace Packages\CmsPackage\Elements\Latte;

interface ILatteElementBackendControlFactory
{

    /**
     * @return LatteElementBackendControl
     */
    public function create();

}