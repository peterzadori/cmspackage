<?php

namespace Packages\CmsPackage\Elements\Latte;

use Packages\CmsPackage\Content\ElementType;

class LatteElement extends ElementType
{

    /**
     * @var ILatteElementBackendControlFactory
     */
    private $backendControlFactory;

    /**
     * @var ILatteElementFrontendControlFactory
     */
    private $frontendControlFactory;


    public function __construct(ILatteElementBackendControlFactory $backendControlFactory, ILatteElementFrontendControlFactory $frontendControlFactory)
    {
        $this->backendControlFactory = $backendControlFactory;
        $this->frontendControlFactory = $frontendControlFactory;
    }


    public function getKey()
    {
        return 'latte';
    }


    public function getLabel()
    {
        return 'Latte Template';
    }


    public function getEntity()
    {
        return new LatteElementEntity();
    }


    public function getBackendControl()
    {
        return $this->backendControlFactory->create();
    }


    public function getFrontendControl()
    {
        return $this->frontendControlFactory->create();
    }

}