<?php

namespace Packages\CmsPackage\Elements\Latte;

interface ILatteElementFrontendControlFactory
{

    /**
     * @return LatteElementFrontendControl
     */
    public function create();

}