<?php

namespace Packages\CmsPackage\Elements\Latte;

use Packages\CmsPackage\Model\Entities\Element;

/**
 * Class LatteElementEntity
 * @package Packages\CmsPackage\Elements\Latte
 *
 * @property string $content m:translate
 */
class LatteElementEntity extends Element
{

}