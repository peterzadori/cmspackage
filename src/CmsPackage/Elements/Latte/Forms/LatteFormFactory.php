<?php

namespace Packages\CmsPackage\Elements\Latte\Forms;

use movi\Application\UI\Form;
use movi\Forms\Controls\Translations;
use movi\Forms\Renderers\BootstrapFormRenderer;
use movi\Forms\TranslationsFormFactory;
use Nette\Forms\Container;

class LatteFormFactory extends TranslationsFormFactory
{

    protected function configure(Form $form)
    {
        $form->renderer->setMode(BootstrapFormRenderer::MODE_HORIZONTAL);

        $form['translations'] = new Translations(function(Container $container) {
            $container->addTextArea('content', 'Latte Template')
                ->setRequired();
        });

        $form->addSubmit('save', 'Save');
    }

}