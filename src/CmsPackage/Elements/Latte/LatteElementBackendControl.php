<?php

namespace Packages\CmsPackage\Elements\Latte;

use Packages\CmsPackage\Content\ElementControl;
use Packages\CmsPackage\Elements\Latte\Forms\LatteFormFactory;

class LatteElementBackendControl extends ElementControl
{

    /**
     * @var LatteFormFactory
     * @inject
     */
    public $latteFormFactory;


    protected function createComponentForm()
    {
        $form = $this->latteFormFactory->create($this->element);
        $form->elementPrototype->class[] = 'ajax';

        $form->onSuccess[] = function() {
            $this->presenter->flashMessage('Template was saved.');
        };

        return $form;
    }

}