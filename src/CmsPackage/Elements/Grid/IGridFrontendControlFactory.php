<?php

namespace Packages\CmsPackage\Elements\Grid;

interface IGridFrontendControlFactory
{

    /**
     * @return GridFrontendControl
     */
    public function create();

}