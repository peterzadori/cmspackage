<?php

namespace Packages\CmsPackage\Elements\Grid;

use Packages\CmsPackage\Content\ElementControl;

class AbstractGridElementControl extends ElementControl
{

    protected $isContainer = false;


    public function getColumns()
    {
        $settings = $this->element->getSettings();
        $columns = $settings->columns;
	    $isCustom = false;

        if ($columns === 'custom') {
            $columns = $settings->custom_columns;
	        $isCustom = true;
        }

        $parts = explode('-', $columns);
        $columns = [];

        foreach ($parts as $i => $part)
        {
            $columns[++$i] = $part;

            if ($part == 12 && !$isCustom) $this->isContainer = true;
        }

        return $columns;
    }


    public function isContainer()
    {
        $this->getColumns();

        return $this->isContainer;
    }

}