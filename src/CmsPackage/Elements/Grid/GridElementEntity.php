<?php

namespace Packages\CmsPackage\Elements\Grid;

use LeanMapper\Filtering;
use LeanMapper\Fluent;
use movi\Model\Query;
use Packages\CmsPackage\Model\Entities\Container;

final class GridElementEntity extends Container
{

    /**
     * @return array
     */
    public function getDefaultSettings()
    {
        return ['columns' => '6-6'];
    }


    /**
     * Returns elements in specified column
     *
     * @param null $column
     * @return array
     * @throws \LeanMapper\Exception\Exception
     */
    public function getElements($column = NULL)
    {
        $this->refresh();

        $elements = [];
        $rows = $this->get('elements', [
            'query' => function(Query $query) use ($column) {
                if ($column) $query->where('@grid_column', $column);
                $query->orderBy('@order ASC');
            }
        ]);

        foreach ($rows as $row)
        {
            $elements[$row->order] = $row;
        }

        return $elements;
    }

}