<?php

namespace Packages\CmsPackage\Elements\Grid;

use Nette\Application\UI\Multiplier;
use Packages\CmsPackage\Content\ElementControl;
use Packages\CmsPackage\Content\ElementTypesManager;
use Packages\CmsPackage\Modules\FrontModule\Components\ElementRenderer\IElementRendererControlFactory;
use Packages\CmsPackage\Modules\FrontModule\Components\ElementsRenderer\IElementsRendererControlFactory;

class GridFrontendControl extends AbstractGridElementControl
{

    /**
     * @var IElementRendererControlFactory
     * @inject
     */
    public $elementRendererControlFactory;

    /**
     * @var IElementsRendererControlFactory
     * @inject
     */
    public $elementsRendererControlFactory;

    /**
     * @var array
     */
    private $elements = [];


    public function beforeRender()
    {
        $this->template->element = $this->element;
    }


    public function attached($parent)
    {
        parent::attached($parent);

        if ($this->isContainer()) {
            $this->elements[0] = iterator_to_array($this->element);
        } else {
            foreach ($this->getColumns() as $index => $column)
            {
                $this->elements[$index] = $this->element->getElements($index);
            }
        }
    }


    protected function createComponentElements()
    {
        return new Multiplier(function($column) {
            return $this->elementsRendererControlFactory->create($this->elements[$column]);
        });
    }

}