<?php

namespace Packages\CmsPackage\Elements\Grid;

use Nette\Application\UI\Multiplier;
use Packages\CmsPackage\Content\ElementControl;
use Packages\CmsPackage\Content\ElementTypesManager;
use Packages\CmsPackage\Model\Entities\Element;
use Packages\CmsPackage\Model\Facades\ElementsFacade;
use Packages\CmsPackage\Modules\BackModule\Components\Elements\ElementsControlTrait;
use Packages\CmsPackage\Modules\BackModule\Components\ElementRenderer\IElementRendererControlFactory;
use Packages\CmsPackage\Modules\BackModule\Components\ElementTypePicker\IElementTypePickerControlFactory;
use Packages\CmsPackage\Services\ElementManagement;

class GridBackendControl extends AbstractGridElementControl
{

    use ElementsControlTrait;


    /**
     * @var ElementsFacade
     */
    private $elementsFacade;

    /**
     * @var ElementManagement
     */
    private $elementManagement;

    /**
     * @var IElementTypePickerControlFactory
     * @inject
     */
    public $typePickerControlFactory;

    /**
     * @var IElementRendererControlFactory
     * @inject
     */
    public $elementRendererControlFactory;


    public function __construct(ElementsFacade $elementsFacade, ElementManagement $elementManagement)
    {
        $this->elementsFacade = $elementsFacade;
        $this->elementManagement = $elementManagement;
    }


    public function handleReorder($column)
    {
        $elements = $this->presenter->getHttpRequest()->getQuery('elements');

        $this->elementManagement->onBeforeSort[] = function(Element $element) use ($column) {
            $element->grid_column = $column;
        };

        $this->elementManagement->saveElementsOrder($elements, NULL, $this->element);

        $this->presenter->terminate();
    }


    public function renderPreview()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/GridBackendControlPreview.latte');

        $this->element->refresh();

        $template->element = $this->element;

        $template->render();
    }


    protected function createComponentElementTypePicker()
    {
        return new Multiplier(function() {
            $control = $this->typePickerControlFactory->create();

            $control->onAdd[] = function($elementType) use ($control) {
                $column = $control->getName();
                $order = count($this->getColumnElements($column)) + 1;

                // Set proper element column
                $this->elementManagement->onBeforeAdd[] = function(Element $element) use ($column) {
                    $element->grid_column = $column;
                };

                $element = $this->elementManagement->addElement($elementType, $this->element->route, $this->element, $order);

                $this->getElementsControl()->setElement($element);
            };

            return $control;
        });
    }


    protected function createComponentElement()
    {
        return new Multiplier(function($element) {
            $renderer = $this->elementRendererControlFactory->create($element);
            $renderer->setPreview(true);

            $renderer->onDelete[] = function(Element $element) {
                $elements = $this->getColumnElements($element->grid_column);

                // Reorder elements in column
                foreach ($elements as $row)
                {
                    if ($row->order > $element->order) {
                        $row->order--;
                        $this->elementsFacade->persist($row);
                    }
                }

                $this->elementManagement->deleteElement($element);
            };

            return $renderer;
        });
    }


    /**
     * @param $column
     * @return array
     */
    public function getColumnElements($column)
    {
        return $this->element->getElements($column) ? $this->element->getElements($column) : [];
    }

}