<?php

namespace Packages\CmsPackage\Elements\Grid;

use movi\Application\UI\Form;
use Packages\CmsPackage\Content\ElementSettings;

class GridSettingsFactory implements ElementSettings
{

    public function configure(Form $form)
    {
        $form->addGroup();

        $columns = $form->addSelect('columns', 'Columns layout', [
            '12' => '1 column',
            '6-6' => '2 columns (50% / 50%)',
            '4-4-4' => '3 columns (33% / 33% / 33%)',
            '3-3-3-3' => '4 columns (25% / 25% / 25% / 25%)',
            'custom' => 'Custom'
        ]);

        $columns->addCondition($form::EQUAL, 'custom')
            ->toggle('custom');

        $form->addSelect('row_align_items', 'Row Items Alignment', [
            'align-items-start' => 'Top',
            'align-items-end' => 'Bottom',
            'align-items-center' => 'Center',
            'align-items-baseline' => 'Baseline',
            'align-items-stretch' => 'Stretch'
        ])->setPrompt('Select');

        $form->addGroup()->setOption('id', 'custom');

        $form->addText('custom_columns', 'Custom Layout')
            ->addConditionOn($columns, $form::EQUAL, 'custom')
            ->setRequired();
    }

}