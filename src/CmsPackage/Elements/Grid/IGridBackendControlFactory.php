<?php

namespace Packages\CmsPackage\Elements\Grid;

interface IGridBackendControlFactory
{

    /**
     * @return GridBackendControl
     */
    public function create();

}