<?php

namespace Packages\CmsPackage\Elements\Grid;

use Packages\CmsPackage\Content\CustomizableElement;
use Packages\CmsPackage\Content\ElementType;
use Packages\CmsPackage\Content\PreviewableElement;

class GridElement extends ElementType implements PreviewableElement, CustomizableElement
{

    /**
     * @var IGridBackendControlFactory
     */
    private $gridBackendControlFactory;

    /**
     * @var IGridFrontendControlFactory
     */
    private $gridFrontendControlFactory;


    public function __construct(IGridBackendControlFactory $gridBackendControlFactory, IGridFrontendControlFactory $gridFrontendControlFactory)
    {
        $this->gridBackendControlFactory = $gridBackendControlFactory;
        $this->gridFrontendControlFactory = $gridFrontendControlFactory;
    }


    public function getKey()
    {
        return 'grid';
    }


    public function getEntity()
    {
        return new GridElementEntity();
    }


    public function getLabel()
    {
        return 'Grid';
    }


    public function getPanelClass()
    {
        return 'panel-info';
    }


    public function getBackendControl()
    {
        return $this->gridBackendControlFactory->create();
    }


    public function getFrontendControl()
    {
        return $this->gridFrontendControlFactory->create();
    }


    public function getElementSettingsFactory()
    {
        return new GridSettingsFactory();
    }

}