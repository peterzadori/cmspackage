<?php

namespace Packages\CmsPackage\Elements\Section;

use Packages\CmsPackage\Content\ElementControl;
use Packages\CmsPackage\Elements\Section\Forms\SectionFormFactory;

class SectionElementBackendControl extends ElementControl
{

    /**
     * @var SectionFormFactory
     */
    private $formFactory;


    public function __construct(SectionFormFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }


    public function renderPreview()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/SectionElementBackendControlPreview.latte');

        $template->element = $this->element;

        $template->render();
    }
    

    protected function createComponentForm()
    {
        $form = $this->formFactory->create($this->element);
        $form->elementPrototype->class[] = 'ajax';

        $form->onSuccess[] = function() {
            $this->presenter->flashMessage('Element was saved.');
        };

        return $form;
    }

}