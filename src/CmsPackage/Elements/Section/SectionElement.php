<?php

namespace Packages\CmsPackage\Elements\Section;

use Packages\CmsPackage\Content\ElementType;
use Packages\CmsPackage\Content\PreviewableElement;

class SectionElement extends ElementType implements PreviewableElement
{

    private $backendControlFactory;

    private $frontendControlFactory;


    public function __construct(ISectionElementBackendControlFactory $backendControlFactory, ISectionElementFrontendControlFactory $frontendControlFactory)
    {
        $this->backendControlFactory = $backendControlFactory;
        $this->frontendControlFactory = $frontendControlFactory;
    }


    public function getKey()
    {
        return 'section';
    }


    public function getLabel()
    {
        return 'Section';
    }


    /**
     * @return SectionElementEntity
     */
    public function getEntity()
    {
        return new SectionElementEntity();
    }


    /**
     * @return SectionElementBackendControl
     */
    public function getBackendControl()
    {
        return $this->backendControlFactory->create();
    }


    /**
     * @return SectionElementFrontendControl
     */
    public function getFrontendControl()
    {
        return $this->frontendControlFactory->create();
    }

}