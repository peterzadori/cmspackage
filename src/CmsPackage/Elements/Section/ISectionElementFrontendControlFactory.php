<?php

namespace Packages\CmsPackage\Elements\Section;

interface ISectionElementFrontendControlFactory
{

    /**
     * @return SectionElementFrontendControl
     */
    public function create();

}