<?php

namespace Packages\CmsPackage\Elements\Section;

interface ISectionElementBackendControlFactory
{

    /**
     * @return SectionElementBackendControl
     */
    public function create();

}