<?php

namespace Packages\CmsPackage\Elements\Section;

use Packages\CmsPackage\Content\ElementControl;

class SectionElementFrontendControl extends ElementControl
{

    public function render()
    {
        echo \Nette\Utils\Html::el('div')->id($this->element->section_id);
    }

}