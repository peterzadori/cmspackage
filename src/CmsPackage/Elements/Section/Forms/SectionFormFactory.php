<?php

namespace Packages\CmsPackage\Elements\Section\Forms;

use movi\Application\UI\Form;
use movi\Forms\Controls\Translations;
use movi\Forms\Renderers\BootstrapFormRenderer;
use movi\Forms\TranslationsFormFactory;
use Nette\Forms\Container;

class SectionFormFactory extends TranslationsFormFactory
{

    protected function configure(Form $form)
    {
        $form->renderer->setMode(BootstrapFormRenderer::MODE_HORIZONTAL);

        $form['translations'] = new Translations(function(Container $container) {
            $container->addText('section_name', 'Section Name')
                ->setRequired();
        });

        $form->addText('section_id', 'ID of section')
            ->setRequired();

        $form->addSubmit('save', 'Save');
    }

}