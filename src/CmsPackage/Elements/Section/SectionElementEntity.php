<?php

namespace Packages\CmsPackage\Elements\Section;

use Packages\CmsPackage\Model\Entities\Element;

/**
 * Class SectionElementEntity
 * @package Packages\CmsPackage\Elements\Section
 *
 * @property string $section_name m:size(64) m:translate
 * @property string $section_id m:size(64)
 */
class SectionElementEntity extends Element
{

}