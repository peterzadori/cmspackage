<?php

namespace Packages\CmsPackage\Routes;

use Packages\CmsPackage\Content\RouteType;
use Packages\CmsPackage\Model\Entities\Storage;

class StorageType extends RouteType
{

    public function getKey()
    {
        return 'storage';
    }


    public function getLabel()
    {
        return 'Storage';
    }


    public function getEntity()
    {
        return new Storage();
    }

}