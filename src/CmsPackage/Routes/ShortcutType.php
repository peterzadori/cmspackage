<?php

namespace Packages\CmsPackage\Routes;

use Packages\CmsPackage\Content\RouteType;
use Packages\CmsPackage\Model\Entities\Page;
use Packages\CmsPackage\Model\Entities\Shortcut;

class ShortcutType extends RouteType
{

    public function getKey()
    {
        return 'shortcut';
    }


    public function getLabel()
    {
        return 'Shortcut';
    }


    public function getEntity()
    {
        return new Shortcut();
    }

}