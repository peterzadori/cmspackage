<?php

namespace Packages\CmsPackage\Routes;

use Packages\CmsPackage\Content\RouteType;
use Packages\CmsPackage\Model\Entities\Page;

class PageType extends RouteType
{

    public function getKey()
    {
        return 'page';
    }


    public function getLabel()
    {
        return 'Page';
    }


    public function getEntity()
    {
        return new Page();
    }

}