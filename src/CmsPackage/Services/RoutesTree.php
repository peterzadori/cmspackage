<?php

namespace Packages\CmsPackage\Services;

use Packages\CmsPackage\Model\Entities\Route;
use movi\Model\Facades\TreeFacade;
use movi\Tree\Node;
use movi\Tree\Tree;

class RoutesTree extends Tree
{

	/** @var RouteNode|NULL */
	private $homepage;

	/** @var array */
	private $aliases;

	protected $nodeClass = 'Packages\CmsPackage\Services\RouteNode';


	public function __construct(TreeFacade $facade)
	{
		parent::__construct($facade);

		$this->onFetch[] = function(self $tree, Route $route, RouteNode $node) {
			if (!empty($route->alias)) {
				$this->aliases[$route->alias] = $node;
			}

			if ($route->homepage) {
				$this->homepage = $node;
			}
		};
	}


	public function markAsHomepage(Node $node)
	{
		if ($this->homepage !== NULL) {
			$entity = $this->homepage->entity;
			$entity->homepage = false;

			$this->facade->persist($entity);
		}

		$entity = $node->entity;
		$entity->homepage = true;

		$this->facade->persist($entity);
	}


	public function getHomepage()
	{
		$this->build();

		return $this->homepage;
	}


	public function findByAlias($alias)
	{
		if (isset($this->aliases[$alias])) {
			return $this->aliases[$alias];
		}

		return NULL;
	}


	public function createNode(Node $parent = NULL)
	{
		if ($parent === NULL) $parent = $this->getRoot();

		$entity = new Route();
		$entity->added = new \DateTime();
		$entity->name = 'Nová stránka';
		$entity->parent = $parent->entity;
		$entity->order = count($parent->children) + 1;

		$this->facade->persist($entity);

		return new $this->nodeClass($entity);
	}

} 