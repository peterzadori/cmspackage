<?php

namespace Packages\CmsPackage\Services;

use Nette\Object;
use Packages\CmsPackage\Content\ElementType;
use Packages\CmsPackage\Content\ElementTypesManager;
use Packages\CmsPackage\Model\Entities\Element;
use Packages\CmsPackage\Model\Entities\ReorderElements;
use Packages\CmsPackage\Model\Entities\Route;
use Packages\CmsPackage\Model\Facades\ElementsFacade;

class ElementManagement extends Object
{

    /**
     * @var ElementsFacade
     */
    private $elementsFacade;

    private $elementTypesManager;


    public $onBeforeAdd;

    public $onBeforeSort;


    public function __construct(ElementsFacade $elementsFacade, ElementTypesManager $elementTypesManager)
    {
        $this->elementsFacade = $elementsFacade;
        $this->elementTypesManager = $elementTypesManager;
    }


    public function addElement(ElementType $elementType, Route $route = NULL, $parent = NULL, $order = NULL, $block = NULL)
    {
        $element = $elementType->getEntity();
        $element->entity = get_class($element);
        $element->type = $elementType->getKey();
        $element->settings = $element->getDefaultSettings();
        $element->block = $block;

        if ($parent !== NULL) {
            $parent = $this->elementsFacade->findOne($parent);

            $element->parent = $parent;
            $element->order = $order ? $order : count($parent->elements) + 1;
        } else if ($block) {
            $element->route = $route;

            if (isset($route->blocks[$block])) {
                $element->order = $order ? $order : count($route->blocks[$block]) + 1;
            } else {
                $element->order = 1;
            }
        } else {
            $element->route = $route;
            $element->order = $order ? $order : count($route->elements) + 1;
        }

        $this->onBeforeAdd($element);

        $this->elementsFacade->persist($element);
        
        return $element;
    }


    public function deleteElement(Element $element)
    {
        if ($element->route) {
            $parent = $element->route;
        } else {
            $parent = $element->parent;
        }

        if ($parent instanceof ReorderElements) {
            if (!$element->block && isset($parent->blocks[$element->block])) {
                foreach ($parent->blocks[$element->block] as $row)
                {
                    if ($row->order > $element->order) {
                        $row->order--;
                        $this->elementsFacade->persist($row);
                    }
                }
            } else {
                foreach ($parent->elements as $row)
                {
                    if ($row->order > $element->order) {
                        $row->order--;
                        $this->elementsFacade->persist($row);
                    }
                }
            }
        }

        $this->elementsFacade->delete($element);

        $parent->refresh();
    }


    public function saveElementsOrder(array $order, Route $route = NULL, Element $parent = NULL)
    {
        /** @var Element[] $elements */
        $elements = array_map(function($id) {
            return $this->elementsFacade->findOne($id);
        }, $order);

        foreach ($elements as $order => $element)
        {
            $element->route = $route;
            $element->parent = $parent;
            $element->order = ++$order;

            $this->onBeforeSort($element);

            $this->elementsFacade->persist($element);
        }
    }

}