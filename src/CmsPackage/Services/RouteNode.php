<?php

namespace Packages\CmsPackage\Services;

use movi\Model\Entities\TreeEntity;
use movi\Tree\Node;

final class RouteNode extends Node
{

    /** @var bool */
	private $homepage = false;


	public function getChildrenFromParent()
	{
		return $this->hasChildren() && $this->entity->show_children ? $this->children : (!$this->parent->isRoot() ? $this->parent->children : array());
	}


	public function isHomepage()
	{
		return $this->homepage;
	}


	public function getPath()
	{
		return $this->isHomepage() ? NULL : parent::getPath();
	}


	public function setEntity(TreeEntity $entity)
	{
		if ($entity->homepage) {
			$this->homepage = true;
		}

		parent::setEntity($entity);
	}


    public function getIcon()
    {
        return $this->entity->getIcon();
    }


    public function getPreviousNode()
    {
        $keys = array_keys($this->parent->children);
        $found_index = array_search($this->id, $keys);

        if ($found_index === false || $found_index === 0) {
            return NULL;
        } else {
            $filtered = array_filter(array_slice($this->parent->children, 0, $found_index), function(RouteNode $node) {
                return !$node->entity->hidden;
            });

            return array_pop($filtered);
        }
    }


    public function getNextNode()
    {
        $keys = array_keys($this->parent->children);
        $found_index = array_search($this->id, $keys);

        if ($found_index === false || $found_index === count($keys) - 1) {
            return NULL;
        } else {
            $filtered = array_filter(array_slice($this->parent->children, $found_index + 1), function(RouteNode $node) {
                return !$node->entity->hidden;
            });

            return array_shift($filtered);
        }
    }

} 