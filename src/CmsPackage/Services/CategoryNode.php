<?php

namespace Packages\CmsPackage\Services;

use movi\Tree\Node;

class CategoryNode extends Node
{

    public function getIcon()
    {
        return 'fa-folder-o';
    }

}