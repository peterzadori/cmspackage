<?php

namespace Packages\CmsPackage\Services;

use movi\Files\File;
use movi\Files\FilesManager;
use movi\Forms\Controls\IUploader;
use Nette\Http\FileUpload;

final class RouteImageUploader implements IUploader
{

	/** @var \movi\Files\FilesManager */
	private $filesManager;


	public function __construct(FilesManager $filesManager)
	{
		$this->filesManager = $filesManager;
	}


	public function upload(FileUpload $fileUpload)
	{
		if (!empty($fileUpload->name) && $fileUpload->isImage()) {
			$file = new File($fileUpload->sanitizedName, $fileUpload->contents);
			$file->setNamespace('routes');

			$this->filesManager->write($file);

			return $file->getKey();
		}
	}

}