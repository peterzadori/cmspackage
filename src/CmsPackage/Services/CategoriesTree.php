<?php

namespace Packages\CmsPackage\Services;

use movi\Tree\Tree;

class CategoriesTree extends Tree
{

    protected $nodeClass = 'Packages\CmsPackage\Services\CategoryNode';

}