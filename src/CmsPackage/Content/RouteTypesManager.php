<?php

namespace Packages\CmsPackage\Content;

use Nette\Object;

class RouteTypesManager extends Object
{

    /**
     * @var RouteType[]
     */
    private $types = [];


    public function addType(RouteType $type)
    {
        $this->types[$type->getKey()] = $type;
    }


    public function getType($key)
    {
        return $this->types[$key];
    }


    public function getTypeByEntity($entity)
    {
        foreach ($this->types as $type)
        {
            if (get_class($type->getEntity()) === $entity) {
                return $type;
            }
        }
    }


    public function getTypes()
    {
        return $this->types;
    }

}