<?php

namespace Packages\CmsPackage\Content;

use movi\Application\UI\Form;

interface ElementSettings
{

    public function configure(Form $form);

}