<?php

namespace Packages\CmsPackage\Content;

interface CustomizableElement
{

    public function getElementSettingsFactory();

}