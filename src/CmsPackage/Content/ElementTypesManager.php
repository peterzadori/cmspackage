<?php

namespace Packages\CmsPackage\Content;

use movi\InvalidArgumentException;
use Nette\Object;

final class ElementTypesManager extends Object
{

	/** @var ElementType[] */
	private $types;


	/**
	 * @param ElementType $type
	 * @throws InvalidArgumentException
	 */
	public function addType(ElementType $type)
	{
		if (isset($this->types[$type->getKey()])) {
			throw new InvalidArgumentException("Element '{$type->getKey()}' exists.");
		}

		$this->types[$type->getKey()] = $type;
	}


	/**
	 * @param $key
	 * @return ElementType
	 * @throws InvalidArgumentException
	 */
	public function getType($key)
	{
		if (!isset($this->types[$key])) {
			throw new InvalidArgumentException("Unknown element '$key'.");
		}

		return $this->types[$key];
	}


	/**
	 * @return ElementType[]
	 */
	public function getTypes()
	{
		return $this->types;
	}

} 