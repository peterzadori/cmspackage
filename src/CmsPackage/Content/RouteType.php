<?php

namespace Packages\CmsPackage\Content;

abstract class RouteType
{

    abstract public function getKey();

    abstract public function getLabel();

    abstract public function getEntity();

}