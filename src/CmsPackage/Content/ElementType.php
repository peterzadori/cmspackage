<?php

namespace Packages\CmsPackage\Content;

use Nette\Object;
use Packages\CmsPackage\Model\Entities\Element;
use Packages\CmsPackage\Modules\BackModule\Components\DummyBackendElement\DummyBackendElementControl;
use Packages\CmsPackage\Modules\FrontModule\Components\DummyFrontendElement\DummyFrontendElementControl;

abstract class ElementType extends Object
{

    abstract public function getKey();

    /**
     * @return Element
     */
    abstract public function getEntity();


    /**
     * Visual
     */
    public function getLabel()
    {

    }


    public function getIcon()
    {

    }


    public function getPanelClass()
    {
        return 'panel-default';
    }


    /**
     * Internal
     */

    /**
     * @return ElementControl
     */
    public function getBackendControl()
    {
        return new DummyBackendElementControl();
    }


    /**
     * @return ElementControl
     */
    public function getPreviewControl()
    {

    }


    /**
     * @return ElementControl
     */
    public function getFrontendControl()
    {
        return new DummyFrontendElementControl();
    }


    public function isPreviewable()
    {
        return $this instanceof PreviewableElement;
    }


    public function isCustomizable()
    {
        return $this instanceof CustomizableElement;
    }


    public function isEditable()
    {
        return !$this->getBackendControl() instanceof DummyBackendElementControl || $this->isCustomizable();
    }

}