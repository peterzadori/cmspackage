<?php

namespace Packages\CmsPackage\Content;

use movi\Application\UI\Control;
use Packages\CmsPackage\Model\Entities\Element;

abstract class ElementControl extends Control
{

    /** @var Element */
    protected $element;


    public function setElement(Element $element)
    {
        $this->element = $element;
    }


    public function createTemplate()
    {
        $theme = $this->presenter->context->getByType('Packages\AppPackage\Themes\Theme');
        $template = parent::createTemplate();
        $reflection = $this->getReflection();
        $file = $reflection->getShortName() . '.latte';

        $files = array();
        $files[] = $theme->elementsDir . "/$file";
        $files[] = dirname($reflection->fileName) . '/' . $reflection->getShortName() . '.latte';

        foreach ($files as $file)
        {
            if (file_exists($file)) {
                $template->setFile($file);

                break;
            }
        }

        $template->element = $this->element;
        $template->settings = $this->element->getSettings();

        return $template;
    }

}