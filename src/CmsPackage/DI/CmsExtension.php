<?php

namespace Packages\CmsPackage\DI;

use movi\DI\CompilerExtension;

final class CmsExtension extends CompilerExtension
{

	const CMS_ELEMENT_TAG = 'cms.element.type',
        CMS_ROUTE_TAG = 'cms.route.type';


	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('elementManager'))
			->setClass('Packages\CmsPackage\Content\ElementTypesManager');

        $builder->addDefinition($this->prefix('routeTypesManager'))
            ->setClass('Packages\CmsPackage\Content\RouteTypesManager');
	}


	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();
		$elementManager = $builder->getDefinition($this->prefix('elementManager'));

		foreach (array_keys($builder->findByTag(self::CMS_ELEMENT_TAG)) as $service)
		{
			$elementManager->addSetup('addType', ['@' . $service]);
		}

        $routeTypesManager = $builder->getDefinition($this->prefix('routeTypesManager'));

        foreach (array_keys($builder->findByTag(self::CMS_ROUTE_TAG)) as $service)
        {
            $routeTypesManager->addSetup('addType', ['@' . $service]);
        }
	}

} 