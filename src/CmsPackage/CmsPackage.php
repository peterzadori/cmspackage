<?php

namespace Packages\CmsPackage;

use movi\DI\CompilerExtension;
use movi\Packages\IPackage;
use movi\Packages\Providers\IAssetsProvider;
use movi\Packages\Providers\IConfigProvider;
use movi\Packages\Providers\IExtensionProvider;
use movi\Packages\Providers\IMappingProvider;

class CmsPackage extends CompilerExtension implements IPackage, IConfigProvider, IExtensionProvider, IMappingProvider, IAssetsProvider
{

	public function getConfigFiles()
	{
		return array(
			__DIR__ . '/Resources/config/package.neon'
		);
	}


	public function getExtensions()
	{
		return array(
			'cms' => 'Packages\CmsPackage\DI\CmsExtension'
		);
	}


	public function getMappings()
	{
		return array(
			'Cms' => 'Packages\CmsPackage\Modules\*Module\Presenters\*Presenter'
		);
	}


	public function getAssets()
	{
		return [
			'js' => [
				'back' => [
					'files' => [
						__DIR__ . '/Resources/assets/movi.cms.js'
					]
				]
			],
            'css' => [
                'back' => [
                    'files' => [
                        __DIR__ . '/Resources/assets/cms.css'
                    ]
                ]
            ]
		];
	}

} 