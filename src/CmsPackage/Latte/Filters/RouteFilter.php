<?php

namespace Packages\CmsPackage\Latte\Filters;

use movi\Tree\Node;
use Nette\Object;
use Packages\CmsPackage\Model\Entities\Shortcut;
use Packages\CmsPackage\Services\RoutesTree;

class RouteFilter extends Object
{

    /**
     * @var RoutesTree
     */
    private $routesTree;


    public function __construct(RoutesTree $routesTree)
    {
        $this->routesTree = $routesTree;
    }


    public function getRoute($node)
    {
        if (!$node instanceof Node) {
            $route = $this->routesTree->getNode($node);

            if ($route === NULL) {
                $route = $this->routesTree->findByAlias($node);
            }
        } else {
            $route = $node;
        }

        if ($route) {
            // Shortcut route
            if ($route->getEntity() instanceof Shortcut && $route->getEntity()->shortcut) {
                $route = $this->routesTree->getNode($route->getEntity()->shortcut->id);
            }
        }

        return $route;
    }

}