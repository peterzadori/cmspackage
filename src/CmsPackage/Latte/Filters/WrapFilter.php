<?php

namespace Packages\CmsPackage\Latte\Filters;

use artem_c\emmet\Emmet;
use Nette\Object;
use Packages\CmsPackage\Model\Entities\Element;

class WrapFilter extends Object
{

	public function wrapControl($output, Element $element)
	{
		if ($element->wrapper_html) {
			$wrapper = $element->wrapper_html;

			return (new Emmet($wrapper))->create([
				'output' => $output
			]);
		} else {
			return $output;
		}
	}

}