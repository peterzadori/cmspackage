<?php

namespace Packages\CmsPackage\Latte\Macros;

use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;

class CmsMacros extends MacroSet
{

	public static function install(Compiler $compiler)
	{
		$set = new static($compiler);

		$set->addMacro('route', array($set, 'macroRoute'));
	}


	public function macroRoute(MacroNode $node, PhpWriter $writer)
	{
		return $writer->write('echo $control->presenter->link("//:Cms:Front:Page:view", array("node" => $template->route(%node.args)))');
	}

} 