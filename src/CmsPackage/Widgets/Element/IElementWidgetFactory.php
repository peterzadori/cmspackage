<?php

namespace Packages\CmsPackage\Widgets\Element;

use movi\Components\Widgets\IWidgetFactory;

interface IElementWidgetFactory extends IWidgetFactory
{

    /**
     * @return ElementWidget
     */
    public function create();

}