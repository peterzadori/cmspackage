<?php

namespace Packages\CmsPackage\Widgets\Element;

use movi\Components\Widgets\Widget;
use Packages\CmsPackage\Modules\FrontModule\Components\ElementRenderer\IElementRendererControlFactory;

class ElementWidget extends Widget
{

    /**
     * @var IElementRendererControlFactory
     */
    private $elementRendererControlFactory;


    public function __construct(IElementRendererControlFactory $elementRendererControlFactory)
    {
        $this->elementRendererControlFactory = $elementRendererControlFactory;
    }


    protected function createComponent($name)
    {
        return $this->elementRendererControlFactory->create($name);
    }

}