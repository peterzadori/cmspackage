<?php

namespace Packages\CmsPackage\Widgets\Sections;

use movi\Components\Widgets\IWidgetFactory;

interface ISectionsWidgetFactory extends IWidgetFactory
{

    /**
     * @return SectionsWidget
     */
    public function create();

}