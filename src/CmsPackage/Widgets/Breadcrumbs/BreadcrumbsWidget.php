<?php

namespace Packages\CmsPackage\Widgets\Breadcrumbs;

use movi\Application\UI\NodePresenter;
use movi\Components\Widgets\Widget;
use movi\Tree\Node;

class BreadcrumbsWidget extends Widget
{

    /**
     * @var Node
     */
    private $node;


    public function attached($presenter)
    {
        if ($presenter instanceof NodePresenter) {
            $this->node = $presenter->node;
        }
    }


    public function beforeRender()
    {
        $this->template->parents = $this->node->getParents();
        $this->template->node = $this->node;
    }

}