<?php

namespace Packages\CmsPackage\Widgets\Breadcrumbs;

use movi\Components\Widgets\IWidgetFactory;

interface IBreadcrumbsWidgetFactory extends IWidgetFactory
{

    /**
     * @return BreadcrumbsWidget
     */
    public function create();

}