<?php

namespace Packages\CmsPackage\Widgets\Navigation;

use Packages\CmsPackage\Modules\FrontModule\Presenters\RoutePresenter;
use Packages\CmsPackage\Services\RoutesTree;
use movi\Components\Widgets\Widget;

class NavigationWidget extends Widget
{

    /** @var RoutesTree */
	private $routesTree;

	private $active = array();


	public function __construct(RoutesTree $routesTree)
	{
		$this->routesTree = $routesTree;
	}


	public function attached($presenter)
	{
		parent::attached($presenter);

		if ($presenter instanceof RoutePresenter) {
			$node = $presenter->node;
			$active = array();

			foreach ($node->getParents() as $parent)
			{
				$active[$parent->id] = true;
			}

			$active[$node->id] = true;
			$this->active = $active;
		}
	}


	public function render($showChildren = true, $showHidden = true)
	{
		$this->template->showChildren = $showChildren;
		$this->template->showHidden = $showHidden;

		parent::render();
	}


	public function beforeRender()
	{
		$this->template->nodes = $this->routesTree->getRoot()->children;
		$this->template->active = $this->active;
	}

} 