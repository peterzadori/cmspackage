<?php

namespace Packages\CmsPackage\Widgets\Navigation;

use movi\Components\Widgets\IWidgetFactory;

interface INavigationWidgetFactory extends IWidgetFactory
{

	/** @return NavigationWidget */
	public function create();

} 